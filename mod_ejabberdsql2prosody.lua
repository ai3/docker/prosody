-- Prosody IM
-- Copyright (C) 2008-2010 Matthew Wild
-- Copyright (C) 2008-2010 Waqas Hussain
-- Copyright (C) 2022	  Blallo
--
-- This project is MIT/X11 licensed. Please see the
-- COPYING file in the source package for more information.
--

package.path = package.path ..";../?.lua";
package.cpath = package.cpath..";../?.so";

local my_name = arg[0];
if my_name:match("[/\\]") then
	package.path = package.path..";"..my_name:gsub("[^/\\]+$", "../?.lua");
	package.cpath = package.cpath..";"..my_name:gsub("[^/\\]+$", "../?.so");
end

local serialize = require "util.serialization".serialize;
local st = require "util.stanza";
local sm = require "core.storagemanager";
local parse_xml = require "util.xml".parse;
local stz = require "util.stanza";
local unpack = table.unpack or unpack; --luacheck: ignore 113/unpack
package.loaded["util.logger"] = {init = function() return function() end; end}

-- utility functions --
local function dump(o)
   if type(o) == 'table' then
	  local s = '{ '
	  for k,v in pairs(o) do
		 if type(k) == 'number' then k = '"'..k..'"' end
		 s = s .. '['..k..'] = ' .. dump(v) .. ','
	  end
	  return s .. '} '
   else
	  return tostring(o)
   end
end

function PrettyDump(o)
		return DoPrettyDump(o, 1)
end

function DoPrettyDump(o, nest_level)
	local s = nil
	if type(o) == "table" then
		s = "{"
		local set = false
		local pad = string.rep("\t", nest_level)
		for k, v in pairs(o) do
			s = s .. "\n" .. pad .. tostring(k) .. ": " .. DoPrettyDump(v, nest_level + 1) .. ","
			set = true
		end
		if set then
			s = s .. "\n" .. string.rep("\t", nest_level - 1)
		end
		s = s .. "}"
	else
		s = tostring(o)
	end

	return s
end
-- end: utility functions --

local function format_jid(row)
	return row.username.."@"..row.server_host
end

local function find_timestamp_1(stanza)
	local last_child = stanza.tags[#stanza.tags];
	if not last_child or last_child ~= stanza[#stanza] then
		module:log("warning", "Last child of offline message is not a tag");
		return nil
	end
	if last_child.name ~= "x" and last_child.attr.xmlns ~= "jabber:x:delay" then
		module:log("warning", "Last child of offline message is not a timestamp");
		return nil
	end
	return last_child.attr.stamp
end

local function find_timestamp_2(stanza)
	for i, v in ipairs(stanza.tags) do
		if v.name == "delay" then
			return i, v.attr.stamp
		end
	end
	return 0, nil
end

local random = math.random
math.randomseed(os.time())

-- from: https://gist.github.com/jrus/3197011
local function generate_id()
	local template ='xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
	return string.gsub(template, '[xy]', function (c)
		local v = (c == 'x') and random(0, 0xf) or random(8, 0xb)
		return string.format('%x', v)
	end)
end

local function find_or_generate_id(stanza)
	for _, v in ipairs(stanza.tags) do
		if v.attr then
			if v.attr.id then
				return v.attr.id
			end
		end
	end

	local id, _ = generate_id()
	return id
end

local function parseFile(filename)
	local file = nil;
	local last = nil;
	local line = 1;
	local function read(expected)
		local ch;
		if last then
			ch = last; last = nil;
		else
			ch = file:read(1);
			if ch == "\n" then line = line + 1; end
		end
		if expected and ch ~= expected then error("expected: "..expected.."; got: "..(ch or "nil").." on line "..line); end
		return ch;
	end
	local function peek()
		if not last then last = read(); end
		return last;
	end

	local escapes = {
		["\\0"] = "\0";
		["\\'"] = "'";
		["\\\""] = "\"";
		["\\b"] = "\b";
		["\\n"] = "\n";
		["\\r"] = "\r";
		["\\t"] = "\t";
		["\\Z"] = "\26";
		["\\\\"] = "\\";
		["\\%"] = "%";
		["\\_"] = "_";
	}
	local function unescape(s)
		return escapes[s] or error("Unknown escape sequence: "..s);
	end
	local function readString()
		read("'");
		local s = "";
		while true do
			local ch = peek();
			if ch == "\\" then
				s = s..unescape(read()..read());
			elseif ch == "'" then
				break;
			else
				s = s..read();
			end
		end
		read("'");
		return s;
	end
	local function readNonString()
		local s = "";
		while true do
			if peek() == "," or peek() == ")" then
				break;
			else
				s = s..read();
			end
		end
		return tonumber(s);
	end
	local function readItem()
		if peek() == "'" then
			return readString();
		else
			return readNonString();
		end
	end
	local function readTuple()
		local items = {}
		read("(");
		while peek() ~= ")" do
			table.insert(items, readItem());
			if peek() == ")" then break; end
			read(",");
		end
		read(")");
		return items;
	end
	local function readTuples()
		if peek() ~= "(" then read("("); end
		local tuples = {};
		while true do
			table.insert(tuples, readTuple());
			if peek() == "," then read() end
			if peek() == ";" then break; end
		end
		return tuples;
	end
	local function readTableName()
		local tname = "";
		while peek() ~= "`" do tname = tname..read(); end
		return tname;
	end
	local function readInsert()
		if peek() == nil then return nil; end
		for ch in ("INSERT INTO `"):gmatch(".") do -- find line starting with this
			if peek() == ch then
				read(); -- found
			else -- match failed, skip line
				while peek() and read() ~= "\n" do end
				return nil;
			end
		end
		local tname = readTableName();
		read("`"); read(" ") -- expect this
		if peek() == "(" then -- skip column list
			repeat until read() == ")";
			read(" ");
		end
		for ch in ("VALUES "):gmatch(".") do read(ch); end -- expect this
		local tuples = readTuples();
		read(";"); read("\n");
		return tname, tuples;
	end

	local function readFile(fname)
		file = io.open(fname);
		if not file then error("File not found: "..fname); os.exit(0); end
		local t = {};
		while true do
			local tname, tuples = readInsert();
			if tname then
				if t[tname] then
					local t_name = t[tname];
					for i=1,#tuples do
						table.insert(t_name, tuples[i]);
					end
				else
					t[tname] = tuples;
				end
			elseif peek() == nil then
				break;
			end
		end
		return t;
	end

	return readFile(filename);
end

local NULL = {};

local function roster(dm, row, item)
	local username = format_jid(row);
	local r = dm:get(username) or {};
	r[row.jid] = item;
	local _, err = dm:set_keys(username, r);
	if err then
		module:log("error", "roster (%s - %s): %s", username, row.jid, err);
	else
		module:log("info", "roster (%s - %s): success", username, row.jid);
	end
end

local function roster_pending(dm, row)
	local username = format_jid(row);
	local r = dm:get(username) or {};
	r.pending = r.pending or {};
	r.pending[row.jid] = true;
	local _, err = dm:set_keys(username, r);
	if err then
		module:log("error", "roster-pending (%s - %s): %s", username, row.jid, err);
	else
		module:log("info", "roster-pending (%s - %s): success", username, row.jid);
	end
end

local function roster_group(dm, row)
	local username = format_jid(row);
	local r = dm:get(username) or {};
	local item = r[row.jid];
	if not item then
		module:log("warning", "No roster item %s for user %s, can't put in group %s", row.jid, username, row.grp);
		return;
	end
	item.groups[row.grp] = true;
	local _, err = dm:set_keys(username, r);
	if err then
		module:log("error", "roster-group (%s - %s - %s): %s", username, row.jid, row.grp, err);
	else
		module:log("info", "roster-group (%s - %s - %s): success", username, row.jid, row.grp);
	end
end

local function private_storage(dm, row, stanza)
	local username = format_jid(row);
	local private = dm:get(username) or {};
	private[stanza.name..":"..row.namespace] = st.preserialize(stanza);
	local _, err = dm:set_keys(username, private);
	if err then
		module:log("error", "private (%s - %s): %s", username, row.namespace, err);
	else
		module:log("info", "private (%s - %s): success", username, row.username);
	end
end

local function offline_msg(dm, username, id, t, stanza)
	stanza.attr.stamp = os.date("!%Y-%m-%dT%H:%M:%SZ", t);
	stanza.attr.stamp_legacy = os.date("!%Y%m%dT%H:%M:%S", t);

	local with = stanza.name;
	if stanza.attr.type then
		with = with .. "<" .. stanza.attr.type;
	end

	local _, err = dm:append(username, id, stz.preserialize(stanza), t, with);
	if err then
        local stanza_dump = PrettyDump(stanza)
		module:log("debug", "stanza pretty dump %s", stanza_dump)
		module:log("error", "offline (%s - %s): %s", username, t, err);
        io.write(("%s"):format(stanza_dump))
	else
		module:log("info", "offline (%s - %s): success", username, t);
	end
end

local function date_parse(s, time_offset)
	local year, month, day, hour, min, sec = s:match("(....)-?(..)-?(..)T(..):(..):(..)");
	return os.time({year=year, month=month, day=day, hour=hour, min=min, sec=sec-time_offset});
end

local function store_accounts(accounts_store, parsed)
	if parsed["users"] and #parsed["users"] > 0 then
		for _, row in ipairs(parsed["users"] or NULL) do
			local username = format_jid(row);
			local _, err = accounts_store:set(format_jid(row), "password", row.password);
			if err then
				module:log("error", "accounts (%s): %s", username, err);
			else
				module:log("info", "accounts (%s): success", username);
			end
		end
	end
end

local function store_rosterusers(roster_store, parsed)
	for _, row in ipairs(parsed["rosterusers"] or NULL) do
		local name = row.nick;
		if name == "" then name = nil; end
		local subscription = row.subscription;
		if subscription == "N" or subscription == "" then
			subscription = "none"
		elseif subscription == "B" then
			subscription = "both"
		elseif subscription == "F" then
			subscription = "from"
		elseif subscription == "T" then
			subscription = "to"
		else
			module:log("warning", "Unknown subscription type: %s", subscription);
			goto continue;
		end;
		local ask = row.ask;
		if ask == "N" then
			ask = nil;
		elseif ask == "O" then
			ask = "subscribe";
		elseif ask == "I" then
			roster_pending(roster_store, row);
			ask = nil;
		elseif ask == "B" then
			roster_pending(roster_store, row);
			ask = "subscribe";
		else
			module:log("warning", "Unknown ask type: %s", ask);
			goto continue;
		end
		local item = {name = name, ask = ask, subscription = subscription, groups = {}};
		roster(roster_store, row, item);
		::continue::
	end
end

local function store_rostergroup(roster_store, parsed)
	for _, row in ipairs(parsed["rostergroups"] or NULL) do
		roster_group(roster_store, row);
	end
end

local function store_vcard(vcard_store, parsed)
	for _, row in ipairs(parsed["vcard"] or NULL) do
		local username = format_jid(row);
		local stanza, err = parse_xml(row.vcard);
		if stanza then
			_, err = vcard_store:set(username, st.preserialize(stanza));
			if err then
				module:log("error", "vCard (%s): %s", username, err);
			else
				module:log("info", "vCard (%s): success", username);
			end
		else
			module:log("error", "vCard XML parse failed (%s)", username);
		end
	end
end

local function store_private(private_store, parsed)
	for _, row in ipairs(parsed["private_storage"] or NULL) do
		local stanza, err = parse_xml(row.data);
		if err then
			module:log("warning", "error in parsing xml: %s", err);
		end
		if stanza then
			private_storage(private_store, row, stanza);
		else
			module:log("error", "Private XML parse failed (%s)", format_jid(row));
		end
	end
end

local function store_offline_msg(offline_store, parsed)
    local file = io.open(("/var/lib/prosody/migration-%s.log"):format(os.date()), "a")
    io.output(file)

	table.sort(parsed["spool"] or NULL, function(a,b) return a.seq < b.seq; end); -- sort by sequence number, just in case

	local time_offset = os.difftime(os.time(os.date("!*t")), os.time(os.date("*t"))) -- to deal with timezones

	for _, row in ipairs(parsed["spool"] or NULL) do
		local username = format_jid(row);
		local stanza, err = parse_xml(row.xml)
		if err then
			module:log("error", "parsing xml failed: %s", err);
			goto continue;
		end
		-- module:log("debug", "stanza: %s", dump(stanza));

		if stanza then
			local stamp = find_timestamp_1(stanza)

			if stamp then
				module:log("warning", "could not find stamp with first approach");
				stanza[#stanza], stanza.tags[#stanza.tags] = nil, nil;
			else
				local i

				i, stamp = find_timestamp_2(stanza)
				if i == 0 then
					module:log("info", "stanza: %s", PrettyDump(stanza));
					module:log("error", "no stamp found");
					goto continue;
				end

				stanza[i], stanza.tags[i] = nil, nil
			end

			local t = date_parse(stamp, time_offset);
			-- local id = find_or_generate_id(stanza);

			offline_msg(offline_store, username, row.seq, t, stanza);
		else
			module:log("error", "Offline message XML parsing failed: %s", username);
		end
		::continue::
	end
    io.close(file)
end

function module.command(arg)
	local help_flags = {"/?", "-?", "?", "/h", "-h", "/help", "-help", "--help"};

	local check_args = function(a)
		for _, flag in ipairs(a) do
			for _, hflag in ipairs(help_flags) do
				if flag == hflag then
					return true
				end
			end
		end

		return false
	end

	if not arg or check_args(arg) or #arg ~= 2 then
		module:log("info", [[ejabberd SQL DB dump importer for Prosody

	  Usage: ejabberdsql2prosody.lua host filename.txt

	The file can be generated using mysqldump:
	  mysqldump db_name > filename.txt]]);
		os.exit(1);
	end

	local host, filename = unpack(arg);

	module:log("debug", "host: %s, filename: %s", host, filename);

	if not prosody.hosts[host] then
		module:log("error", "The host %q is not know by Prosody.", host);
	end

	sm.initialize_host(host);

	local accounts_store = module:open_store("accounts", "map")
	local roster_store = module:open_store("roster", "map");
	local vcard_store = module:open_store("vcard", "map");
	local private_store = module:open_store("private", "map");
	local offline_store = module:open_store("offline", "archive");

	module:log("info", "stores initialized");

	local map = {
		["last"] = {"username", "server_host", "seconds", "state"};
		["privacy_default_list"] = {"username", "server_host", "name"};
		["privacy_list"] = {"username", "server_host", "name", "id", "current_timestamp"};
		["privacy_list_data"] = {"id", "t", "value", "action", "ord", "match_all", "match_iq", "match_message", "match_presence_in", "match_presence_out"};
		["private_storage"] = {"username", "server_host", "namespace", "data", "current_timestamp"};
		["rostergroups"] = {"username", "server_host", "jid", "grp"};
		["rosterusers"] = {"username", "server_host", "jid", "nick", "subscription", "ask", "askmessage", "server", "subscribe", "type", "current_timestamp"};
		["spool"] = {"username", "server_host", "xml", "seq", "current_timestamp"};
		["users"] = {"username", "server_host", "password", "current_timestamp"};
		["vcard"] = {"username", "server_host", "vcard", "current_timestamp"};
		--["vcard_search"] = {};
	}

	module:log("info", "parsing file: %s", filename);
	local parsed = parseFile(filename);

	for name, data in pairs(parsed) do
		local m = map[name];
		if m then
			if #data > 0 and #data[1] ~= #m then
				module:log("warning", "expected %d columns for table `%s`, found %d", #m, name, #data[1]);
			end
			for i=1,#data do
				local row = data[i];
				for j=1,#m do
					row[m[j]] = row[j];
					row[j] = nil;
				end
			end
		end
	end

	store_accounts(accounts_store, parsed)
	store_rosterusers(roster_store, parsed)
	store_rostergroup(roster_store, parsed)
	store_vcard(vcard_store, parsed)
	store_private(private_store, parsed)
    store_offline_msg(offline_store, parsed)
end
