#!/bin/sh
#
# Install script for ejabberd inside a Docker container
# (using Debian packages).
#

# Packages to install.
PACKAGES="
    prosody
    prosody-modules
    lua-dbi-mysql
    lua-unbound
    mercurial
    git
    ca-certificates
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

set -x
set -e

# Install the packages.
apt-get -q update
install_packages ${PACKAGES}

# download community modules
hg clone https://hg.prosody.im/prosody-modules/ /usr/lib/prosody/community-modules

# override with our vendored ones
for MOD in "mod_auth_custom_http" "mod_net_proxy"; do
    rm -rf "/usr/lib/prosody/community-modules/$MOD"
    git clone "https://git.autistici.org/prosody/$MOD" "/usr/lib/prosody/community-modules/$MOD"
done

# Make sure /run has the right permissions.
chmod 1777 /run

# Send logs to stderr by default.
perl -i -pe 'BEGIN{undef $/;} s/^log = {.*?^}$/log = {\n    {levels = {min = "info"}, to = "console"};\n}/smg' /etc/prosody/prosody.cfg.lua
chmod 755 /usr/lib/prosody/community-modules
chmod 644 /etc/prosody/prosody.cfg.lua
chmod 755 /etc/prosody/conf.*

apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
