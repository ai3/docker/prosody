FROM golang:1.23 as builder

WORKDIR /src
RUN go install git.autistici.org/id/auth-server-http-proxy@v0.2.0

FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master

COPY conf /etc/services.d
COPY build.sh /tmp/build.sh
COPY mod_ejabberdsql2prosody.lua /usr/lib/prosody/modules/
COPY --from=builder /go/bin/auth-server-http-proxy /usr/local/bin/
RUN /tmp/build.sh && rm /tmp/build.sh
